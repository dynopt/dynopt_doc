# README #

This is source code to User's Guide for dynopt. Any improvements, corrections, or new examples are welcome.

Development of the package has been moved to github: https://github.com/miroslavfikar/dynopt_doc

Contact: miroslav.fikar@stuba.sk